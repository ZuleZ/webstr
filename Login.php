<?php include 'Connect.php'; ?>

<?php 
session_start();
 
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index.php");
    exit;
}
 
require_once "Connect.php";
 
//Varijable 
$username = $password = "";
$username_err = $password_err = $login_err = "";
 
//FORMA
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
   //Provjere, za uname, pass itd
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    

    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    if(empty($username_err) && empty($password_err)){
       
        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($mysqli, $sql)){
            
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = $username;
            
            
            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);
                
               //Jel postoji uname?
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            session_start();
                            
                            //Sesija
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                        
                            header("location: Welcome.php");
                        } else{
                            $login_err = "Invalid username or password.";
                        }
                    }
                } else{
                    $login_err = "Invalid username or password.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            
            mysqli_stmt_close($stmt);
        }
    }
    mysqli_close($mysqli);
}
?>

<!DOCTYPE html>
<html>

   

<head>
    <link rel="stylesheet" href="Style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200&display=swap" rel="stylesheet">
    <title>Index</title>

</head>


<body>

    <div class="SL">
        <ul>

        <!-- Header dio, kod log-ina. Ako user nije logiran, ne pokazuje mu nista -->
        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])){  ?>
                
                <li><a>Pozdrav<?php echo str_repeat('&nbsp;', 1); echo htmlentities($_SESSION['username']);?>,</a></li>   
                
        
                    <?php } else {  ?>
        
                <li><a href="Login.php">Log-In</a></li>
                <li><a href="Register.php">Sign-Up</a></li>
        
                    <?php } ?>
       </ul>
       </div>
       
    <div class="head">
        <ul>
           <li><a href="index.php">Home</a></li>
           <li><a href="Engines.php">Engines</a></li>
           <li><a href="Forum.php">Gallery</a></li>
           <li><a href="Contact.php">Contact</a></li>
       </ul>
       </div>

       <h2>Log-in</h2>

   
        
    

    <div class="container">

 <!-- Forma za logiranje -->

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label><b>Username:</b></label>
                <input type="text" placeholder="Enter Username" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group">
                <label><b>Password:</b></label>
                <input type="password" placeholder="Enter Password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input id="Sub_but" type="submit" class="btn btn-primary" value="Login">
            </div>
        </form>
    
    </div>

</body>
</html>