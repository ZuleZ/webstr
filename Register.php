<?php

session_start();
require_once "Connect.php";


$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";
 
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    //PROVJERE
    if(empty(trim($_POST["username"]))){
        $username_err = "Molimo unesite Username!";
    } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["username"]))){
        $username_err = "Username moze sadrzavati samo slova i brojeve!";
    } else{
        
        $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($mysqli, $sql)){
            
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = trim($_POST["username"]);
            
           
            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "Username se vec koristi";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Error! Pokusajte ponovno!";
            }

            mysqli_stmt_close($stmt);
        }
    }
    
    //Pass provjera
    if(empty(trim($_POST["password"]))){
        $password_err = "Unesite password";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password mora imati najmanje 6 znakova";
    } else{
        $password = trim($_POST["password"]);
    }
    
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Potvrdite password";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Passwordi se ne slazu";
        }
    }
    
    // Provjera ERRORA kod baze
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
         
        if($stmt = mysqli_prepare($mysqli, $sql)){
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
           
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT);
            
            if(mysqli_stmt_execute($stmt)){
                header("location: Welcome2.php");
            } else{
                echo "Error! Pokusajte ponovno";
            }

            mysqli_stmt_close($stmt);
        }
    }
    
    mysqli_close($mysqli);
}

 ?>

<!DOCTYPE html>
<html>

   

<head>
    <link rel="stylesheet" href="Style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200&display=swap" rel="stylesheet">
    <title>Index</title>

</head>


<body>

    <div class="SL">
        <ul>
        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])){  ?>
                
                <li><a>Pozdrav<?php echo str_repeat('&nbsp;', 1); echo htmlentities($_SESSION['username']);?>,</a></li>   
                
        
                    <?php } else {  ?>
        
                
                    <li><a href="Login.php">Log-In</a></li>
                    <li><a href="Register.php">Sign-Up</a></li>
        
                    <?php } ?>
       </ul>
       </div>
       
    <div class="head">
        <ul>
           <li><a href="index.php">Home</a></li>
           <li><a href="Engines.php">Engines</a></li>
           <li><a href="blog.php">Gallery</a></li>
           <li><a href="Contact.php">Contact</a></li>
       </ul>
       </div>

       <h2>Sign-Up</h2>

   
        
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">


    <div class="container">
 <!-- Samo REGISTER forma -->

    <form id="TXT" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label><b>Username:</b></label>
                <input type="text" placeholder="Enter Username" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group">
                <label><b>Password:</b></label>
                <input type="password" placeholder="Enter Password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <label><b>Confirm Password:</b></label>
                <input type="password" placeholder="Confirm password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $confirm_password; ?>">
                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input id="Sub_but" type="submit" class="btn btn-primary" value="Submit">
            </div>
            
        </form>
    
    </div>

</body>
</html>